import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'intime'})
export class InTimePipe implements PipeTransform {
  transform(value: string): string {
    if (value === '') {
      const newValue = 'in|time';
      return newValue;
    } else {
        return value;
    }
  }
}
