import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { NotificatorService } from '../../core/notificator.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css'],
})

export class NavbarComponent implements OnInit {
    isAdmin = false;

    email = '';

    ngOnInit(): void {
        this.isAdminCheck();
        this.email = this.getUserEmail();
    }

    constructor(
        private router: Router,
        private authService: AuthService,
        private notificator: NotificatorService,
        private route: ActivatedRoute) { }


    private isAdminCheck() {
        if (this.authService.isAdmin()) {
            this.isAdmin = true;
        }
    }

    private getUserEmail() {
        return this.authService.getEmail();
    }
    editClicked() {
        this.router.navigate(['user/profile']);
    }

    allUsers() {
        this.router.navigate(['users'], { relativeTo: this.route });
    }

    logout() {
        try {
            this.authService.logoutUser();
            this.notificator.success('You were logged out successfully!');
            this.router.navigate(['home']);
        } catch (err) {
            console.log(err);
            this.notificator.error(err, 'Cannot log out!');
        }
    }
}
