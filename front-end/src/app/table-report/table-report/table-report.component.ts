import { DeviceModel } from './../models/device.model';
import { Component, OnInit, Input, OnDestroy, ViewChild, EventEmitter, Output, Inject } from '@angular/core';
import { TableReportModel } from '../models/table-report.model';
import { TableReportService } from '../table-report.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MatExpansionPanel, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { EditTableReportComponent } from '../edit-table-report/edit-table-report.component';
import { MapService } from '../map/map-service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CreateChartComponent } from '../create-chart/create-chart.component';
import { DialogService } from '../../core/dialog.service';
import { NotificatorService } from '../../core/notificator.service';
@Component({
  selector: 'app-table-report',
  templateUrl: './table-report.component.html',
  styleUrls: ['./table-report.component.css']
})
export class TableReportComponent implements OnInit, OnDestroy {

  @Output() public deletedReport = new EventEmitter();
  @Output() public devicesEmitter = new EventEmitter();
  @Input() public table;
  private report: TableReportModel;
  private apiSubscription;
  private originId;
  private destinationId;
  private dateToDisplay;
  private periodToDisplay;

  messageFromMap;

  constructor(
    private iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
    private readonly tableReportService: TableReportService,
    public dialog: MatDialog,
    private confirmDialogService: DialogService,
    private notificator: NotificatorService,
    private mapService: MapService
  ) {

    iconRegistry.addSvgIcon(
      'chart',
      sanitizer.bypassSecurityTrustResourceUrl('assets/computer.svg'));
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/edit.svg'));
    iconRegistry.addSvgIcon(
      'garbage',
      sanitizer.bypassSecurityTrustResourceUrl('assets/garbage.svg'));
  }

  ngOnInit(): void {
    this.dateToDisplay = this.tableReportService.getDate(this.table);
    this.periodToDisplay = this.tableReportService.getPeriod(this.table);
    this.report = new TableReportModel();
    this.apiSubscription = this.tableReportService
      .getTableReportApiData(this.table)
      .subscribe(data => {
        this.report = data;
      });
    this.originId = this.table.devices[0].name;
    console.log(this.originId);
    this.destinationId = this.table.devices[this.table.devices.length - 1].name;
    console.log(this.destinationId);
  }

  ngOnDestroy(): void {
    this.apiSubscription.unsubscribe();
  }

  newDeviceInfo() {
    this.mapService.changeDeviceInfo(this.table.devices);
  }

  changeChartDevice(origin, destination) {
    this.tableReportService.deviceChange(`${origin},${destination}`);
    console.log(origin, destination);
  }

  createChart(event: Event) {
    event.stopPropagation();

    const dialogRef = this.dialog
      .open(CreateChartComponent, {
        disableClose: true,
        data: {
          tableid: this.table.id
        }
      });

      const newChartSubscription = dialogRef.componentInstance.newChart.subscribe((result) => {
        this.tableReportService.getTableReportCharts(this.table.id).subscribe((charts) => {
          this.table.chartReports = charts;
        });
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      /*       this.tableReportService.getAllTablesReports()
              .subscribe(data => {
                this.tables = data;
              });
            // newReportSubscription.unsubscribe(); */
    });

  }

  editTableReport(event: Event) {
    event.stopPropagation();

    const dialogRef = this.dialog
      .open(EditTableReportComponent,
        {
          disableClose: true,
          data: {
            id: this.table.id,
            name: this.table.name,
            period: +this.table.endDateInMilliseconds - this.table.startDateInMilliseconds,
            offset: this.table.offset,
            selectedDevices: this.table.devices
          }
        });

    const newReportSubscription = dialogRef.componentInstance.editedReport.subscribe((result) => {
      this.table = result;
      this.apiSubscription = this.tableReportService
        .getTableReportApiData(this.table)
        .subscribe(data => {
          this.report = data;
        });
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      /*       this.tableReportService.getAllTablesReports()
              .subscribe(data => {
                this.tables = data;
              });
            // newReportSubscription.unsubscribe(); */
    });

  }

  deleteTableReport(event: Event) {
    console.log(this.table.id);
    event.stopPropagation();

    this.confirmDialogService.openConfirmDialog('Are you sure you want to remove this table report?')
    .afterClosed()
    .subscribe(res => {
        if (res) {
          this.tableReportService
          .deleteTable(this.table.id)
          .subscribe(() => {
            this.deletedReport.emit(this.table.id);
          });
          this.notificator.success('Table report was successfully removed.');
        }
    });

  }

  handleDeletedChart(event) {
    const chartToRemove = this.table.chartReports.filter(chart => chart.id === event)[0];
    const index = this.table.chartReports.indexOf(chartToRemove);
    this.table.chartReports.splice(index, 1);
}

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.table.chartReports, event.previousIndex, event.currentIndex);
  }

  sendDataToTableReportList() {
    this.newDeviceInfo();
  }
}
