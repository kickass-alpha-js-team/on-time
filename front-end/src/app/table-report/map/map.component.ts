import * as L from 'leaflet';
import { Icon, icon, Marker, marker } from 'leaflet';
import { Component, OnInit, Input } from '@angular/core';
import { map } from 'rxjs/operators';
import { MapService } from './map-service';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css'],
})

export class MapComponent implements OnInit {
    deviceInfo;
    popup;
    map;
    myLat = '42.65778';
    myLong = '23.31445';

    private defaultIcon: Icon = icon({
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
    });
    @Input() deviceInfoFromTableReport;

    constructor(private readonly mapService: MapService) {
        Marker.prototype.options.icon = this.defaultIcon;
    }


    ngOnInit(): void {
        this.mapService.currentInfo
            .subscribe(message => {
                this.deviceInfo = message;
            });

            this.mapService.initializeMap();
        // this.map.invalidateSize();

    }

}


