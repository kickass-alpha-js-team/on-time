import { polyline } from '@amcharts/amcharts4/.internal/core/rendering/Path';
import { DeviceModel } from './../models/device.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import * as L from 'leaflet';

@Injectable()
export class MapService {
    private map;
    private markers = [];
    private latlngs = [];
    private polyline;
    private layerGroup;

    private defaultDeviceModel: DeviceModel = {
        id: 'default',
        name: 'Rythm Engineering Office',
        latitude: '42.66209410387305',
        longitude: '23.3178073170393'
    };

    private deviceInfo: BehaviorSubject<DeviceModel[]> = new BehaviorSubject([this.defaultDeviceModel]);
    currentInfo = this.deviceInfo.asObservable();

    changeDeviceInfo(currentDevices) {
        this.deviceInfo.next(currentDevices);
    }
    initializeMap() {

        this.map = L.map('map')
            .setView(new L.LatLng(this.defaultDeviceModel.latitude, this.defaultDeviceModel.longitude), 14, { animation: true });

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: `Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,
            <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery ©
            <a href="https://www.mapbox.com/">Mapbox</a>`,
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWFyaWFkaW5ldmEiLCJhIjoiY2pyaGlxd3gyMDhrNjN5cDFqOXJvOHlkbiJ9.SBA1G7WTIJk8rAj91qL_-Q'
        }).addTo(this.map);

        this.currentInfo.subscribe(data => this.drawOnTheMap(data));

    }

    private drawOnTheMap(deviceInfo: DeviceModel[]) {
        this.clearPolyline();
        this.clearMarkers();

        deviceInfo.forEach(device => {
            const currentMarker = L.marker([device.latitude, device.longitude]);
            currentMarker
                .bindPopup(`Device name: ${device.name}.<br> Latitude: ${device.latitude}. <br> Longitude: ${device.longitude}`);

            this.markers.push(currentMarker);

            this.latlngs.push(currentMarker.getLatLng());
        });

        this.createLayer();
        this.createPolyline();
    }

    private createPolyline() {
        this.polyline = L.polyline(this.latlngs, { color: 'green' });
        this.map.addLayer(this.polyline);
        this.map.fitBounds(this.polyline.getBounds());
    }

    private clearPolyline() {
        if (this.polyline) {
            this.map.removeLayer(this.polyline);
            this.latlngs = [];
        }
    }

    private clearMarkers() {
        if (this.markers.length >= 1) {
            this.layerGroup.clearLayers();
            this.markers = [];
        } else {
            this.map.removeLayer(this.markers);
            this.markers = [];
        }
    }
    private createLayer() {
        this.layerGroup = L.layerGroup(this.markers)
            .addTo(this.map);
    }

}
