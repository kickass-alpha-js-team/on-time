import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TableReportListComponent } from './table-report-list/table-report-list.component';

const routes: Routes = [
  {
    path: '', component: TableReportListComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TableReportRoutingModule { }

