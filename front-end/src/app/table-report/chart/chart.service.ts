import { Observable } from 'rxjs/Observable';
import { ApiService } from '../../core/api.service';
import { RequesterService } from '../../core/requester.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators/map';

@Injectable()
export class ChartService {
    constructor(
        private readonly apiService: ApiService,
        private readonly requester: RequesterService
    ) { }


    public getChartApiData(originId, destinationId, chart): Observable<any> {
        return this.apiService.chartReport(originId, destinationId, chart);
    }

    public createChart(tableid, newchart) {
        return this.requester.post(`http://localhost:3000/table-reports/${tableid}/chart-reports`, newchart);
    }

    public deleteChart(tableReportId, chartId) {
        return this.requester.delete(
            `http://localhost:3000/table-reports/${tableReportId}/chart-reports/${chartId}`);
    }
}
