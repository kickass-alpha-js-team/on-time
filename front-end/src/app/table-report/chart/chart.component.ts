import { DialogService } from './../../core/dialog.service';
import { Output, Input, OnInit, OnDestroy, Component, NgZone, AfterViewInit, EventEmitter } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TableReportService } from '../table-report.service';
import { MatDialog } from '@angular/material/dialog';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { ChartService } from './chart.service';
import { NotificatorService } from '../../core/notificator.service';

am4core.useTheme(am4themes_animated);


@Component({
  selector: 'app-chart-report',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent implements OnInit, OnDestroy, AfterViewInit {

  private chart: am4charts.XYChart;
  private apiData;

  @Input() originId;
  @Input() destinationId;
  @Input() public chartData;
  @Input() currentTable;
  @Output() public deletedChart = new EventEmitter();

  constructor(
    private zone: NgZone,
    private iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
    private chartService: ChartService,
    private tableReportService: TableReportService,
    public dialog: MatDialog,
    private confirmDialogService: DialogService,
    private notificator: NotificatorService) {

    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/edit.svg'));
    iconRegistry.addSvgIcon(
      'garbage',
      sanitizer.bypassSecurityTrustResourceUrl('assets/garbage.svg'));
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.tableReportService.currentChartDevices$.subscribe(
      message => {
        this.originId = message.split(',')[0] || this.originId;
        this.destinationId = message.split(',')[1] || this.destinationId;
        this.zone.runOutsideAngular(() => {
          const chartDivName = this.chartData.name;
          const chart = am4core.create(chartDivName, am4charts.XYChart);

          // ... chart code ...

          chart.paddingRight = 20;

          this.chartService.getChartApiData(this.originId, this.destinationId, this.chartData).subscribe(resultData => {
            chart.colors.step = 2;
            // Add data
            chart.data = generateChartData();
            // Create axes
            const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.minGridDistance = 50;

            // Create series
            function createAxisAndSeries(field, name, opposite) {
              const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

              const series = chart.series.push(new am4charts.LineSeries());
              series.dataFields.valueY = field;
              series.dataFields.dateX = 'date';
              series.strokeWidth = 2;
              series.yAxis = valueAxis;
              series.name = name;
              series.tooltipText = '{name}: [bold]{valueY}[/]';
              series.tensionX = 0.8;

              const interfaceColors = new am4core.InterfaceColorSet();

              valueAxis.renderer.line.strokeOpacity = 1;
              valueAxis.renderer.line.strokeWidth = 2;
              valueAxis.renderer.line.stroke = series.stroke;
              valueAxis.renderer.labels.template.fill = series.stroke;
              valueAxis.renderer.opposite = opposite;
              valueAxis.renderer.grid.template.disabled = true;
            }

            const startDates = Object.keys(resultData);

            startDates.forEach(date => {
              createAxisAndSeries(date, new Date(+date).toLocaleString(), false);
            });

            // Add legend
            chart.legend = new am4charts.Legend();

            // Add cursor
            chart.cursor = new am4charts.XYCursor();

            // generate some random data, quite different range
            function generateChartData() {
              const chartData = [];

              resultData[Object.keys(resultData)[0]].forEach((point, index) => {
                const chartPoint =
                  Object.keys(resultData).reduce((obj, date) => {
                    obj[date] = resultData[date][index]['y'];
                    return obj;
                  }, {});
                chartPoint['date'] = +point.x;
                chartData.push(chartPoint);
              });

              return chartData;
            }

          });

        });
      });

  }

  ngOnDestroy(): void {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  deleteChart(event: Event) {
    event.stopPropagation();

    this.confirmDialogService.openConfirmDialog('Are you sure you want to remove this chart report?')
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.chartService
            .deleteChart(this.currentTable.id, this.chartData.id)
            .subscribe(() => {
              this.deletedChart.emit(this.chartData.id);
            });

          this.notificator.success('Chart report was successfully removed.');
        }
      });
  }
}
