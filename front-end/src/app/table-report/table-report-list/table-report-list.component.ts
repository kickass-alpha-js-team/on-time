import { OnInit, Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateTableReportComponent } from '../create-table-report/create-table-report.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { TableReportService } from '../table-report.service';


@Component({
    selector: 'app-table-report-list',
    templateUrl: './table-report-list.component.html',
    styleUrls: ['./table-report-list.component.css']

})
export class TableReportListComponent implements OnInit, OnDestroy {

    private tables: any[];
    private tablesSubscription;

    deviceInfoFromTableReport;

    public constructor(
        private readonly tableReportService: TableReportService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
        this.tablesSubscription = this.tableReportService.getAllTablesReports()
            .subscribe(data => {
                this.tables = data;
            });
    }

    ngOnDestroy(): void {
        this.tablesSubscription.unsubscribe();
    }

    public openDialog() {
        const dialogRef = this.dialog.open(CreateTableReportComponent, {
            disableClose: true,
            data: {
                name: '',
                period: '',
                offset: '',
                selectedDevices: '',
            }
        });
        const newReportSubscription = dialogRef.componentInstance.newReport.subscribe((result) => {
            this.tableReportService.getAllTablesReports()
                .subscribe(data => {
                    this.tables = data;
                });
        });
        dialogRef.afterClosed().subscribe(result => {
            // console.log(result);
            // this.tableReportService.getAllTablesReports()
            //     .subscribe(data => {
            //         this.tables = data;
            //     });
            // newReportSubscription.unsubscribe();
        });
    }

    handleDeletedReport(event) {

        const tableToRemove = this.tables.filter(table => table.id === event)[0];
        const index = this.tables.indexOf(tableToRemove);
        this.tables.splice(index, 1);

    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.tables, event.previousIndex, event.currentIndex);
    }

    handleEmitterFromTableReport(event) {
        this.deviceInfoFromTableReport = event;
    }

}
