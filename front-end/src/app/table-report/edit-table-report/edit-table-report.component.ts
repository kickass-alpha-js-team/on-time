import { Component, OnInit, EventEmitter, Output, OnDestroy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequesterService } from '../../core/requester.service';
import { DevicesService } from '../../core/devices.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TableReportService } from '../table-report.service';
import { Observable } from 'rxjs/Observable';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificatorService } from '../../core/notificator.service';


export interface Period {
    name: string;
    value: number;
}
@Component({
    selector: 'app-edit-table',
    templateUrl: './edit-table-report.component.html',
    styleUrls: ['./edit-table-report.component.css']
})

export class EditTableReportComponent implements OnInit, OnDestroy {

    private devicesSubscription;
    private editTableForm: FormGroup;
    public periodName;
    editedReport = new EventEmitter();

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private deviceService: DevicesService,
        private tableReportService: TableReportService,
        private notificator: NotificatorService,
    ) { }

    devices: any[];
    selectedDevices: any[];
    periods: Period[] = [
        { name: '15 Minutes', value: 15  },
        { name: '30 Minutes', value: 30  },
        { name: '45 Minutes', value: 45  },
        { name: '1 Hour', value: 60  },
        { name: '2 Hours', value: 2 * 60  },
        { name: '3 Hours', value: 3 * 60  },
        { name: '4 Hours', value: 4 * 60  },
        { name: '5 Hours', value: 5 * 60  },
        { name: '6 Hours', value: 6 * 60  },
        { name: '7 Hours', value: 7 * 60  },
        { name: '8 Hours', value: 8 * 60  },
        { name: '12 Hours', value: 12 * 60  },
        { name: '24 Hours', value: 24 * 60  },
    ];

    ngOnInit(): void {
        this.selectedDevices = [...this.data.selectedDevices];
        this.devicesSubscription = this.deviceService.fetch().subscribe(data => {
            this.devices = data.filter((device) => {

                return !this.selectedDevices.map(selectedDev => selectedDev.name).includes(device.name);
            });
        });

        this.editTableForm = this.formBuilder.group({
            name: [this.data.name,
                Validators.required],
            period: [null,
                Validators.required],
            offset: [+this.data.offset || 0],
        });

        const periodToSelect = this.periods.find((period) => {
            return period.value === this.data.period / 60000;
        });

        this.editTableForm.get('period').setValue(periodToSelect);
    }

    ngOnDestroy(): void {
        this.devicesSubscription.unsubscribe();
    }

    editTable() {
        const newTableReport = this.transformData();
        this.tableReportService.editTable(this.data.id, newTableReport).subscribe((updatedTable) => {
            this.editedReport.emit(updatedTable);
        });

        this.notificator.success('Table report was successfully edited.');
    }

    transformData() {
        const offset = this.editTableForm.value.offset ?
            { days: 0, hours: +this.editTableForm.value.offset } : { days: 0, hours: 0 };
        const newTableReport = {
            name: this.editTableForm.value.name,
            period: this.editTableForm.value.period.value,
            offset,
            deviceNames: this.selectedDevices.map(device => device.name)
        };
        return newTableReport;
    }

    get reportName() {
        return this.editTableForm.get('name');
    }

    get reportPeriod() {
        return this.editTableForm.get('period');
    }

    get reportOffset() {
        return this.editTableForm.get('offset');
    }

    get reportDevices() {
        return this.editTableForm.get('devices');
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
        }
    }
}
