import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChartService } from '../chart/chart.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent, MAT_DIALOG_DATA } from '@angular/material';
import { NotificatorService } from '../../core/notificator.service';

export interface Fruit {
  name: string;
}

@Component({
  selector: 'app-create-chart',
  templateUrl: './create-chart.component.html',
  styleUrls: ['./create-chart.component.css']
})
export class CreateChartComponent implements OnInit {

  newChart = new EventEmitter();
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  startDateTimes = [
  ];


  todayDate;
  private createChartForm: FormGroup;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private chartService: ChartService,
    private notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.todayDate = new Date();
    this.createChartForm = this.formBuilder.group({
      name: ['', Validators.required],
      period: ['', Validators.required],
    });
  }

  createChart() {
    console.log(this.createChartForm.value);
    console.log(this.startDateTimes);
    this.transformData();
    const newChart = this.transformData();
    console.log(newChart);
    this.chartService
      .createChart(this.data.tableid, newChart)
      .subscribe(() =>
        this.newChart.emit('new chart created')
        );

        this.notificator.success('New chart report was created.');
  }

  transformData() {
    const startDates = this.startDateTimes.map(datetime => {
      const date = datetime.dateTime.split(',')[0];
      // console.log(`date: ${date}`);
      let time = datetime.dateTime.split(',')[1].trim().split(' ')[0];
      // console.log(`time: ${time}`);
      const period = datetime.dateTime.split(',')[1].trim().split(' ')[1];
      // console.log(`period: ${period}`);
      let month = date.split('/')[0];
      // console.log(`month: ${month}`);
      let day = date.split('/')[1];
      // console.log(`day: ${day}`);
      const year = date.split('/')[2];
      // console.log(`year: ${year}`);
      if (month.length === 1) {
        month = `0${month}`;
      }
      if (day.length === 1) {
        day = `0${day}`;
      }
      if (period === 'PM') {
        const hours = time.split(':')[0];
        const minutes = time.split(':')[1];
        time = `${+hours + 12}:${minutes}`;
        // console.log(time);
      }
      if (time.length === 4) {
        time = `0${time}`;
      }

      const dateTimeString = `${year}-${month}-${day}T${time}:00`;
      console.log(`string: ${dateTimeString}`);
      console.log(Date.parse(dateTimeString));
      return Date.parse(dateTimeString);
    });
    const newChart = {
      name: this.createChartForm.value.name,
      periodInMilliseconds: this.createChartForm.value.period * 3600000,
      startDates: startDates
    };
    return newChart;
  }


  get chartName() {
    return this.createChartForm.get('name');
  }

  get chartPeriod() {
    return this.createChartForm.get('period');
  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add chip
    if ((value || '').trim()) {
      this.startDateTimes.push({ dateTime: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(datetime): void {
    const index = this.startDateTimes.indexOf(datetime);

    if (index >= 0) {
      this.startDateTimes.splice(index, 1);
    }
  }
}
