import { NgModule } from '@angular/core';
import { TableReportRoutingModule } from './table-report-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TableReportComponent } from './table-report/table-report.component';
import { TableReportListComponent } from './table-report-list/table-report-list.component';
import { CreateTableReportComponent } from './create-table-report/create-table-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableReportService } from './table-report.service';
import { EditTableReportComponent } from './edit-table-report/edit-table-report.component';
import { ChartComponent } from './chart/chart.component';
import { MapComponent } from './map/map.component';
import { MapService } from './map/map-service';
import { ChartService } from './chart/chart.service';
import { CreateChartComponent } from './create-chart/create-chart.component';
import { AngularSplitModule } from 'angular-split';
import { InTimePipe } from '../pipes/intime.pipe';

@NgModule({
    declarations: [
        InTimePipe,
        TableReportComponent,
        TableReportListComponent,
        CreateChartComponent,
        CreateTableReportComponent,
        EditTableReportComponent,
        ChartComponent,
        MapComponent
    ],
    entryComponents: [
        CreateTableReportComponent, EditTableReportComponent, CreateChartComponent
    ],
    imports: [
        SharedModule,
        TableReportRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        TableReportService,
        MapService,
        ChartService
    ]
})

export class TableReportModule { }
