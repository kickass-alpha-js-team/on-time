import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequesterService } from '../../core/requester.service';
import { DevicesService } from '../../core/devices.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TableReportService } from '../table-report.service';
import { Observable } from 'rxjs/Observable';
import { NotificatorService } from '../../core/notificator.service';
import { of } from 'rxjs/internal/observable/of';


export interface Period {
  name: string;
  value: number;
}
@Component({
  selector: 'app-create-table',
  templateUrl: './create-table-report.component.html',
  styleUrls: ['./create-table-report.component.css']
})

export class CreateTableReportComponent implements OnInit, OnDestroy {

  private devicesSubscription;
  private createTableForm: FormGroup;
  newReport = new EventEmitter();

  devices: any[];
  selectedDevices: any[];

  minSelectedDevices: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private deviceService: DevicesService,
    private tableReportService: TableReportService,
    private notificator: NotificatorService,
  ) { }

  periods: Period[] = [
    { name: '15 Minutes', value: 15 },
    { name: '30 Minutes', value: 30 },
    { name: '45 Minutes', value: 45 },
    { name: '1 Hour', value: 60 },
    { name: '2 Hours', value: 2 * 60 },
    { name: '3 Hours', value: 3 * 60 },
    { name: '4 Hours', value: 4 * 60 },
    { name: '5 Hours', value: 5 * 60 },
    { name: '6 Hours', value: 6 * 60 },
    { name: '7 Hours', value: 7 * 60 },
    { name: '8 Hours', value: 8 * 60 },
    { name: '12 Hours', value: 12 * 60 },
    { name: '24 Hours', value: 24 * 60 },
  ];

  ngOnInit(): void {
    this.selectedDevices = [];
    this.devicesSubscription = this.deviceService.fetch().subscribe(data => {
      this.devices = data;
    });
    this.createTableForm = this.formBuilder.group({
      name: ['',
        Validators.required],
      period: ['',
        Validators.required],
      offset: [''],
    });
  }

  ngOnDestroy(): void {
    this.devicesSubscription.unsubscribe();
  }

  createTable() {
    const newTableReport = this.transformData();
    this.tableReportService
      .createTable(newTableReport)
      .subscribe(() =>
        this.newReport.emit(''));

    this.notificator.success('New table report was created.');
  }

  transformData() {
    const offset = this.createTableForm.value.offset ?
      { days: 0, hours: +this.createTableForm.value.offset } : { days: 0, hours: 0 };
    const newTableReport = {
      name: this.createTableForm.value.name,
      period: this.createTableForm.value.period.value,
      offset,
      deviceNames: this.selectedDevices.map(device => device.name)
    };
    return newTableReport;
  }

  get reportName() {
    return this.createTableForm.get('name');
  }

  get reportPeriod() {
    return this.createTableForm.get('period');
  }

  get reportOffset() {
    return this.createTableForm.get('offset');
  }

  get reportDevices() {
    return this.createTableForm.get('devices');
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    of(this.selectedDevices.length).subscribe(length => {
      console.log(length);
      if (length < 2) {
        this.minSelectedDevices = false;
      } else {
        this.minSelectedDevices = true;
      }
    });
  }
}
