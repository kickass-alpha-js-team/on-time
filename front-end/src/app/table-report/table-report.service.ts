import { Injectable } from '@angular/core';
import { RequesterService } from '../core/requester.service';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class TableReportService {
    constructor(
        private readonly apiService: ApiService,
        private readonly requester: RequesterService
    ) { }

    private chartDevicesEmitter$ = new BehaviorSubject('');
    currentChartDevices$ = this.chartDevicesEmitter$.asObservable();

    deviceChange(message) {
        this.chartDevicesEmitter$.next(message);
    }

    public getPeriod(table) {
        const periodInMinutes = (table.endDateInMilliseconds - table.startDateInMilliseconds) / 60000;
        return periodInMinutes < 60 ? `Period: ${periodInMinutes} Minutes` : `Period: ${periodInMinutes / 60} Hours`;
    }

    public getDate(table) {
        const date = new Date(+table.endDateInMilliseconds).toLocaleString();
        return date;
    }

    public getAllTablesReports(): Observable<any> {
        return this.requester
            .get('http://localhost:3000/table-reports');
    }

    public getTableReportApiData(table): Observable<any> {
        return this.apiService.tableReport(table);
    }

    public getTableReportCharts(tableReportId): Observable<any> {
        return this.requester.get(`http://localhost:3000/table-reports/${tableReportId}/chart-reports`);
    }

    public createTable(newTableReport) {
        return this.requester.post(
            'http://localhost:3000/table-reports', newTableReport);
    }

    public deleteTable(tableId) {
        return this.requester.delete(
            `http://localhost:3000/table-reports/${tableId}`);
    }

    public editTable(tableId, editedTableData) {
        return this.requester.put(
            `http://localhost:3000/table-reports/${tableId}`, editedTableData);
    }
}
