export class DeviceModel {
    id: string;
    name: string;
    longitude: string;
    latitude: string;
}
