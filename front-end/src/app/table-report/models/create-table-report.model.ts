import { Offset } from '@progress/kendo-angular-popup';

export class CreateTableReportModel {

  name: string;

  period: number;

  offset?: Offset;

  deviceNames: string[];
}
