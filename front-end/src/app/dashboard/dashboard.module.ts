import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { NavbarComponent } from '../components/nav/navbar.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
    declarations: [
        DashboardComponent,
        NavbarComponent,
    ],
    imports: [
        SharedModule,
        DashboardRoutingModule,
    ]
})

export class DashboardModule { }
