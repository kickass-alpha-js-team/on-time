import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
    {
        path: '', component: DashboardComponent,
        children: [{
            path: 'users',
            loadChildren: '../view-users/view-users.module#ViewUsersModule',
            pathMatch: 'full'
        }, {
            path: '',
            loadChildren: '../table-report/table-report.module#TableReportModule',
            pathMatch: 'full'
        }, {
            path: 'devices',
            loadChildren: '../devices/devices.module#DevicesModule',
            pathMatch: 'full'
        }, {
            path: 'audit-log',
            loadChildren: '../audit-logger/audit-logger.module#AuditLoggerModule',
            pathMatch: 'full'
        }]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
