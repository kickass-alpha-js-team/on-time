import { AdminRouteGuardService } from './../core/route-guards/admin-route-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewUsersComponent } from './view-users.component';

const routes: Routes = [
  {
    path: '', component: ViewUsersComponent,
    pathMatch: 'full', canActivate: [AdminRouteGuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewUsersRoutingModule { }

