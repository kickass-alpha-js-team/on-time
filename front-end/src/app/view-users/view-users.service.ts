import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { tap } from 'rxjs/operators/tap';
import { RequesterService } from '../core/requester.service';

@Injectable()
export class ViewUsersService extends BehaviorSubject<any[]> {
    constructor(private requester: RequesterService) {
        super([]);
    }

    private data = [];

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
                tap(data => {
                    this.data = data;
                })
            )
            .subscribe(data => {
                super.next(data);
            });
    }

    public save(data: any) {
        this.requester.post(`http://localhost:3000/users`, JSON.stringify(data))
        .subscribe();

        this.reset();

        this.fetch()
            .subscribe(() => this.read(), () => this.read());
    }

    public remove(id: string) {
        this.requester.delete(`http://localhost:3000/users/${id}`).subscribe();

        this.reset();

        this.fetch()
            .subscribe(() => this.read(), () => this.read());
    }

    private reset() {
        this.data = [];
    }

    private fetch(): Observable<any[]> {
        return this.requester.get(`http://localhost:3000/users`);
    }

}
