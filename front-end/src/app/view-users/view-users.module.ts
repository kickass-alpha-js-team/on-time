import { NgModule } from '@angular/core';
import { ViewUsersComponent } from './view-users.component';
import { SharedModule } from '../shared/shared.module';
import { ViewUsersRoutingModule } from './view-users-routing.module';
import { ViewUsersService } from './view-users.service';
import { GridModule } from '@progress/kendo-angular-grid/dist/es2015/grid.module';

@NgModule({
  declarations: [ViewUsersComponent],
  imports: [
    SharedModule,
    ViewUsersRoutingModule,
    GridModule,
    SharedModule,
  ],
  providers: [
    ViewUsersService,
  ]
})
export class ViewUsersModule { }
