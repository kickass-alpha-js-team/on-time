import { Component, OnInit, Inject } from '@angular/core';
import { process, State } from '@progress/kendo-data-query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserListModel } from '../user/models/user-list.model';
import { ViewUsersService } from './view-users.service';
import { map } from 'rxjs/operators';
import { DialogService } from '../core/dialog.service';
import { NotificatorService } from '../core/notificator.service';
import { Observable } from 'rxjs/Observable';
import { GridDataResult } from '@progress/kendo-angular-grid/dist/es2015/data/data.collection';

@Component({
    selector: 'app-view-users',
    templateUrl: './view-users.component.html',
})
export class ViewUsersComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    private editedRowIndex: number;

    public show = false;
    constructor(@Inject(ViewUsersService) private viewUsersService,
        private dialogService: DialogService,
        private readonly notificator: NotificatorService) {
    }

    public ngOnInit(): void {
        this.viewUsersService.pipe(map(data => process(data as any, this.gridState))).subscribe(data => this.view = data);

        this.viewUsersService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;

        this.viewUsersService.read();
    }

    public addHandler({ sender }) {

        this.show = true;

        this.closeEditor(sender);

        this.formGroup = new FormGroup({
            'email': new FormControl('', Validators.required),
            'password': new FormControl('', Validators.required),
        });

        sender.addRow(this.formGroup);
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);

        this.show = false;
    }

    public saveHandler({ sender, rowIndex, formGroup }) {
        const user: UserListModel = formGroup.value;

        this.viewUsersService.save(user);

        sender.closeRow(rowIndex);

        this.notificator.success('User was successfully added.');

        this.show = false;
    }

    public removeHandler({ dataItem }) {
        if (dataItem.isAdmin) {
            this.notificator.error('You cannot delete administrator.');
        } else {
            this.dialogService.openConfirmDialog('Are you sure you want to delete this user?')
                .afterClosed()
                .subscribe(res => {
                    if (res) {
                        this.viewUsersService.remove(dataItem.id);
                        this.notificator.success('User was successfully deleted.');
                    }
                });
        }
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
}
