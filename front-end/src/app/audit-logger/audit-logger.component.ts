import { Component, OnInit, Inject } from '@angular/core';
import { RequesterService } from '../core/requester.service';
import { process, State } from '@progress/kendo-data-query';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuditLoggerService } from './audit-logger.service';
@Component({
    selector: 'app-audit-logger',
    templateUrl: './audit-logger.component.html',
    // styleUrls: ['./view-users.component.css']
})

export class AuditLoggerComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 15
    };
    constructor(@Inject(AuditLoggerService) private auditLoggerService) { }

    public ngOnInit(): void {
        this.auditLoggerService.pipe(map(data => process(data as any, this.gridState)))
        .subscribe(data => this.view = data);

        this.auditLoggerService.read();

    }

    public onStateChange(state: State) {
        this.gridState = state;

        this.auditLoggerService.read();
    }

}
