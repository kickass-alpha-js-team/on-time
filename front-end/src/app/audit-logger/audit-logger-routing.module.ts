import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditLoggerComponent } from './audit-logger.component';
import { AdminRouteGuardService } from '../core/route-guards/admin-route-guard.service';

const routes: Routes = [
    {
      path: '', component: AuditLoggerComponent,
      pathMatch: 'full', canActivate: [AdminRouteGuardService]
    },
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

export class AuditLoggerRoutingModule { }
