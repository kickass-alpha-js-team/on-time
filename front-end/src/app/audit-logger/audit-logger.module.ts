import { AuditLoggerService } from './audit-logger.service';
import { NgModule } from '@angular/core';
import { AuditLoggerComponent } from './audit-logger.component';
import { SharedModule } from '../shared/shared.module';
import { AuditLoggerRoutingModule } from './audit-logger-routing.module';
import { GridModule } from '@progress/kendo-angular-grid';

@NgModule({
    declarations: [
        AuditLoggerComponent,
    ],
    imports: [
        SharedModule,
        GridModule,
        AuditLoggerRoutingModule,
    ],
    providers: [
        AuditLoggerService,
    ],
})

export class AuditLoggerModule { }
