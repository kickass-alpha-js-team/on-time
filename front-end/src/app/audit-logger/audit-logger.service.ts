import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { tap } from 'rxjs/operators/tap';
import { RequesterService } from '../core/requester.service';

@Injectable()
export class AuditLoggerService extends BehaviorSubject<any[]> {
    constructor(private requester: RequesterService) {
        super([]);
    }

    private data = [];

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
                tap(data => {
                    this.data = data;
                })
            )
            .subscribe(data => {
                super.next(data);
            });
    }

    private fetch(): Observable<any[]> {
        return this.requester.get(`http://localhost:3000/audit-logger`);
    }
}