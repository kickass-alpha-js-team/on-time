import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { NotificatorService } from '../../core/notificator.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from './password-validation.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    registerForm: FormGroup;
    maxLength = 30;
    minLength = 5;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private readonly notificator: NotificatorService,
        // private passValidator: PasswordValidation,
    ) { }

    ngOnInit(): void {

        this.loginForm = this.formBuilder.group({
            email: ['',
            [Validators.required, Validators.email, Validators.maxLength(this.maxLength), Validators.minLength(this.minLength)]],
            password: ['',
            [Validators.required, Validators.maxLength(this.maxLength), Validators.minLength(this.minLength)]]
        });

        this.registerForm = this.formBuilder.group({
            email: ['',
            [Validators.required, Validators.email, Validators.maxLength(this.maxLength), Validators.minLength(this.minLength)]],
            password: ['',
            [Validators.required, Validators.maxLength(this.maxLength), Validators.minLength(this.minLength)]],
            passwordRepeat: ['', [Validators.required, Validators.maxLength(this.maxLength), Validators.minLength(this.minLength)]]
        }, {
        validator: PasswordValidation.MatchPassword
    });

    }

    login() {
        this.authService.loginUser(this.loginForm.value).subscribe(
            () => {
                this.notificator.success('Logged in successfully!');
                  this.router.navigate(['intime']);
            },
            error => {
                this.notificator.error(error, 'Login failed!');
            }
        );
    }

    register() {
        this.authService.registerUser(this.registerForm.value).subscribe(
            () => {
                this.notificator.success('Congratulations! Registration was successful! You can now sign in.');
                this.router.navigate(['home']);
            },
            error => {
                this.notificator.error(error, 'Registration failed!');
            }
        );
    }

    checkPassword(pass, pass2) {
        if (pass !== pass2) {
            this.notificator.error('Error', 'Passwords don`t match.');
            throw new Error('Passwords don`t match.');
        }
    }
    get loginEmail() {
        return this.loginForm.get('email');
    }

    get loginPass() {
        return this.loginForm.get('password');
    }

    get email() {
        return this.registerForm.get('email');
    }
    get password() {
        return this.registerForm.get('password');
    }

    get passwordRepeat() {
        return this.registerForm.get('passwordRepeat');
    }
    cancel() {
        this.router.navigate(['home']);
    }
}
