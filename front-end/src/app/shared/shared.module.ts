import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UnderLineMeDirective } from '../directives/underline.directive';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ModalComponent } from './modal/modal.component';
import {MatIconModule} from '@angular/material/icon';
import {MatSliderModule} from '@angular/material/slider';
import {MatChipsModule} from '@angular/material/chips';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { LayoutModule } from '@progress/kendo-angular-layout';

@NgModule({
  declarations: [
    UnderLineMeDirective,
    ModalComponent
  ],
  imports: [
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatChipsModule,
    MatSliderModule,
    MatIconModule,
    DragDropModule,
    MatFormFieldModule,
    MatSelectModule,
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatExpansionModule,
    MatDialogModule,
  ],
  exports: [
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatChipsModule,
    MatSliderModule,
    MatIconModule,
    DragDropModule,
    MatFormFieldModule,
    MatSelectModule,
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    UnderLineMeDirective,
    MatExpansionModule,
    MatDialogModule,
    ModalComponent
  ]
})
export class SharedModule { }
