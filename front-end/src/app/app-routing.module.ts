import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthRouteActivatorService } from './core/route-guards/auth-route-activator.service';

export const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full'},
    { path: 'home', redirectTo: '' },
    { path: 'not-found', component: NotFoundComponent },
    { path: 'intime', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthRouteActivatorService]},

    { path: '**', redirectTo: '/not-found', pathMatch: 'full' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy:  PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
