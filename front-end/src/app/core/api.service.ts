import { RequesterService } from './requester.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ApiService {
  constructor(
    private readonly requester: RequesterService
  ) { }

  test() {
    return this.requester.get('hi');
  }

  tableReport(report): Observable<any> {
    const devices: string = report.devices.map(x => x.name).join(',');
    // tslint:disable-next-line:object-literal-key-quotes
    const period = `{"from": ${report.startDateInMilliseconds},"to": ${report.endDateInMilliseconds}}`;
    const url = `http://ec2-35-158-53-19.eu-central-1.compute.amazonaws.com:8080/api/travelTimeTableData?devices=${devices}&date=${period}`;
    const result = this.requester.get(url);
    return result;
  }

  chartReport(originID, destinationID, chart): Observable<any> {
    const startDates: string = chart.startDates.map(startDate => startDate.dateInMilliseconds).join(',');
    const period: number = chart.periodInMilliseconds;
    // tslint:disable-next-line:max-line-length
    const url = `http://ec2-35-158-53-19.eu-central-1.compute.amazonaws.com:8080/api/comparePeriods?originDeviceId=${originID}&destinationDeviceId=${destinationID}&startDates=${startDates}&periodLength=${period}`;
    const result = this.requester.get(url);
    return result;
  }
}
