import { AdminRouteGuardService } from './route-guards/admin-route-guard.service';
import { AnonymousRouteGuardService } from './route-guards/anonimous-route-guard.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NotificatorService } from './notificator.service';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';
import { RequesterService } from './requester.service';
import { AuthRouteActivatorService } from './route-guards/auth-route-activator.service';
import { ApiService } from './api.service';
import { DevicesService } from './devices.service';
import { DialogService } from './dialog.service';

@NgModule({
  providers: [
    NotificatorService,
    AuthService,
    StorageService,
    NotificatorService,
    RequesterService,
    AnonymousRouteGuardService,
    AuthRouteActivatorService,
    AdminRouteGuardService,
    ApiService,
    DevicesService,
    DialogService,
  ]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
