import * as jwt_decode from 'jwt-decode';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { RequesterService } from './requester.service';
import { UserModel } from '../user/models/user.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators/tap';

@Injectable()
export class AuthService {
  currentUser;
  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.hasToken()
  );

  public constructor(
    private readonly storageService: StorageService,
    private readonly requester: RequesterService
  ) { }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public registerUser(user: UserModel): Observable<any> {
    return this.requester.post(
      'http://localhost:3000/register',
      user
    );
  }

  public loginUser(user: UserModel): Observable<any> {

    return this.requester
      .post('http://localhost:3000/login', user).pipe(
        tap(response => {
          this.storageService.setItem('token', (<any>response).token);
          this.isLoggedInSubject$.next(true);
        })
      );
  }

  public logoutUser() {
    this.storageService.removeItem('token');
    this.isLoggedInSubject$.next(false);
  }

  private hasToken(): boolean {
    return !!this.storageService.getItem('token');
  }
  private getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  isAuthenticated() {
    return !!this.currentUser;
  }

  isAdmin(): boolean {
    const decodedToken = this.getDecodedAccessToken(this.storageService.getItem('token'));

    return !!decodedToken ? decodedToken.isAdmin : false;
  }

  getEmail(): string {
    const decodedToken = this.getDecodedAccessToken(this.storageService.getItem('token'));

    return decodedToken.email;
  }

  updateCurrentUser(firstName: string, lastName: string) {
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;
  }
}
