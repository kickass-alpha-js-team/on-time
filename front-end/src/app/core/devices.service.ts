import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { tap } from 'rxjs/operators/tap';
import { RequesterService } from './requester.service';


@Injectable()
export class DevicesService extends BehaviorSubject<any[]> {
    constructor(private requester: RequesterService) {
        super([]);
    }

    private data: any[] = [];

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
                tap(data => {
                    this.data = data;
                })
            )
            .subscribe(data => {
                super.next(data);
            });
    }

    public save(data: any, isNew?: boolean, id?: string) {

        if (isNew === true) {
        this.requester.post(`http://localhost:3000/devices`, JSON.stringify(data))
            .subscribe();
        } else {
            this.requester.put(`http://localhost:3000/devices/${id}`, data).subscribe();
        }

        this.reset();

        this.fetch()
            .subscribe(() => this.read(), () => this.read());
    }

    public remove(id: string) {
        this.requester.delete(`http://localhost:3000/devices/${id}`).subscribe();

        this.reset();

        this.fetch()
            .subscribe(() => this.read(), () => this.read());
    }

    private reset() {
        this.data = [];
    }

    public fetch(): Observable<any[]> {
        return this.requester.get(`http://localhost:3000/devices`);
    }

}
