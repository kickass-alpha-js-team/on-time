import { AuthService } from '../auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificatorService } from 'src/app/core/notificator.service';

@Injectable()
export class AdminRouteGuardService implements CanActivate {
  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) { }

  public canActivate(): boolean {
    if (this.authService.isAdmin() === false) {
      this.notificator.error(
        'You must be administrator in order to see this page!'
      );

      this.router.navigate(['/intime']);

      return false;
    }

    return true;
  }
}
