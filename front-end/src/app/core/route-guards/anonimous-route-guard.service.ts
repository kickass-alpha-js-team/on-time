import { AuthService } from '../auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificatorService } from 'src/app/core/notificator.service';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';

@Injectable()
export class AnonymousRouteGuardService implements CanActivate {
  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) {}

  public canActivate(): Observable<boolean> {
    return this.authService.isLoggedIn$.pipe(
      map((isLogged: boolean) => {
        if (isLogged) {
          this.router.navigate(['/home']);
          this.notificator.error(
            'You must not be logged-in in order to access this page!'
          );
        }

        return !isLogged;
      })
    );
  }
}
