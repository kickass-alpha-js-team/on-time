import { Injectable } from '@angular/core';
import { ModalComponent } from '../shared/modal/modal.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable()

export class DialogService {
    constructor (private dialog: MatDialog) {

    }

    openConfirmDialog(msg) {
        return this.dialog.open(ModalComponent, {
            width: '390px',
            panelClass: 'confirm-dialog-container',
            disableClose: true,
            data: {
                message: msg
            }
        });
    }
}
