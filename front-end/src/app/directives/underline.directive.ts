import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appUnderlineMe]'
})
export class UnderLineMeDirective  {
  constructor(private elRef: ElementRef, private renderer: Renderer2) {
    const el = this.elRef.nativeElement;
  }

  @HostListener('mouseenter')
  in() {
    const el = this.elRef.nativeElement;
    this.renderer.setStyle(el, 'border-bottom' , '0.1rem solid #5cb85c');
  }


  @HostListener('mouseleave')
  out() {
    const el = this.elRef.nativeElement;
    this.renderer.setStyle(el, 'border-bottom', 'none');
  }

}
