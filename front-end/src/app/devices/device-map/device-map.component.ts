import * as L from 'leaflet';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Icon, icon, Marker, marker } from 'leaflet';

@Component({
    selector: 'app-device-map',
    templateUrl: './device-map.component.html',
    styleUrls: ['./device-map.component.css'],
})

export class DeviceMapComponent implements OnInit {
    deviceMap;
    marker;
    private initialLat = '42.66209410387305';
    private initialLong = '23.3178073170393';

    private defaultIcon: Icon = icon({
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
    });

    @Output() sendCoordinates: EventEmitter<object> = new EventEmitter();

    @Input() deviceInfoFromParent;

    ngOnInit(): void {
        Marker.prototype.options.icon = this.defaultIcon;
        this.initializeMap();

        if (this.deviceInfoFromParent.name) {
            this.addMarker(this.deviceInfoFromParent.latitude, this.deviceInfoFromParent.longitude);
            this.centerMapAroundMarker(this.deviceInfoFromParent.latitude, this.deviceInfoFromParent.longitude);
        }

        this.getLatitudeLongitude();
    }

    initializeMap() {
        this.deviceMap = L.map('deviceMap').setView(new L.LatLng(this.initialLat, this.initialLong), 14, { animation: true });
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: `Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,
             <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery ©
             <a href="https://www.mapbox.com/">Mapbox</a>`,
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWFyaWFkaW5ldmEiLCJhIjoiY2pyaGlxd3gyMDhrNjN5cDFqOXJvOHlkbiJ9.SBA1G7WTIJk8rAj91qL_-Q'
        }).addTo(this.deviceMap);
    }

    getLatitudeLongitude() {
        this.deviceMap.on('click', (e: L.MouseEvent) => {
            this.sendCoordinates.emit({
                newLat: e.latlng.lat,
                newLong: e.latlng.lng,
            });

            this.clearMap();

            this.marker = new L.marker(e.latlng).addTo(this.deviceMap);
        });
    }

    addMarker(lat, long) {
        this.clearMap();

        this.marker = new L.marker([lat, long]).addTo(this.deviceMap);
    }

    centerMapAroundMarker(lat, long) {
        this.deviceMap.panTo(new L.LatLng(lat, long));
    }

    clearMap() {
        if (this.marker !== undefined) {
            this.deviceMap.removeLayer(this.marker);
        }
    }
}


