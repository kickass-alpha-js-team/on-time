import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { GridModule } from '@progress/kendo-angular-grid/dist/es2015/grid.module';
import { DevicesComponent } from './devices.component';
import { DevicesRoutingModule } from './devices-routing.module';
import { DeviceModalComponent } from './device-modal/device-modal.component';
import { DeviceModalService } from './device-modal/device-modal.service';
import { DeviceMapComponent } from './device-map/device-map.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  providers: [
    DeviceModalService
  ],
  declarations: [
    DevicesComponent,
    DeviceModalComponent,
    DeviceMapComponent,
  ],
  imports: [
    SharedModule,
    DevicesRoutingModule,
    GridModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  entryComponents: [DeviceModalComponent]
})
export class DevicesModule { }
