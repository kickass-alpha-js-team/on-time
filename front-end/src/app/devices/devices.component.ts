import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserListModel } from '../user/models/user-list.model';
import { map } from 'rxjs/operators';
import { DevicesService } from '../core/devices.service';
import { DialogService } from '../core/dialog.service';
import { NotificatorService } from '../core/notificator.service';
import { DeviceModalService } from './device-modal/device-modal.service';

@Component({
    selector: 'app-devices',
    templateUrl: './devices.component.html',
})
export class DevicesComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    private editedRowIndex: number;
    private editClicked = false;

    constructor(@Inject(DevicesService) private devicesService,
        private dialogService: DialogService,
        private deviceModalService: DeviceModalService,
        private readonly notificator: NotificatorService) { }

    public ngOnInit(): void {
        this.devicesService.pipe(map(data => process(data as any, this.gridState))).subscribe(data => this.view = data);

        this.devicesService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;

        this.devicesService.read();
    }

    public addHandler() {

        this.deviceModalService.openConfirmDialog('Add a new device.')
            .afterClosed()
            .subscribe(res => {
                if (res) {
                    this.notificator.success('Device was successfully added.');
                }
            });
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.editClicked = true;
        this.closeEditor(sender);

        this.formGroup = new FormGroup({
            'name': new FormControl(dataItem.name),
            'longitude': new FormControl(dataItem.longitude, Validators.required),
            'latitude': new FormControl(dataItem.latitude),
        });

        this.editedRowIndex = rowIndex;

        sender.editRow(rowIndex, this.formGroup);

        this.deviceModalService.openEditDialog(`Edit device with name ${dataItem.name}`, this.formGroup.value, dataItem.id)
        .afterClosed()
        .subscribe(res => {
            if (res) {
                this.cancelHandler({sender, rowIndex});
                this.notificator.success('Device was successfully edited.');
            }
            this.cancelHandler({sender, rowIndex});
        });
    }

    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({ sender, rowIndex, formGroup, isNew, dataItem }) {
        const user: UserListModel = formGroup.value;

        this.devicesService.save(user, isNew, dataItem.id);

        sender.closeRow(rowIndex);
    }

    public removeHandler({ dataItem }) {
        this.dialogService.openConfirmDialog('Are you sure you want to remove this device?')
            .afterClosed()
            .subscribe(res => {
                if (res) {
                    this.devicesService.remove(dataItem.id);
                    this.notificator.success('Device was successfully removed.');
                }
            });
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
}
