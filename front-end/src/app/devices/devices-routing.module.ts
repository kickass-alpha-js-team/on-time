import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DevicesComponent } from './devices.component';
import { AdminRouteGuardService } from '../core/route-guards/admin-route-guard.service';

const routes: Routes = [
  {
    path: '', component: DevicesComponent,
    pathMatch: 'full', canActivate: [AdminRouteGuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevicesRoutingModule { }
