import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeviceModalComponent } from './device-modal.component';

@Injectable()

export class DeviceModalService {

    constructor (private dialog: MatDialog) {

    }

    openConfirmDialog(msg) {
        return this.dialog.open(DeviceModalComponent, {
            disableClose: true,
            data: {
                message: msg,
            }
        });
    }

    openEditDialog(msg, deviceData, deviceId) {
        return this.dialog.open(DeviceModalComponent, {
            disableClose: true,
            data: {
                message: msg,
                prePopulatedData: deviceData,
                id: deviceId,
            }
        });
    }
}
