import { DevicesService } from './../../core/devices.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, Inject, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { formGroupNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { DeviceModalService } from './device-modal.service';

@Component({
    selector: 'app-device-modal',
    templateUrl: './device-modal.component.html',
    styleUrls: ['./device-modal.component.css'],
})
export class DeviceModalComponent implements OnInit {

    deviceInfo: FormGroup;
    dataToEdit = false;


    @Output() sendDeviceInfo: EventEmitter<object> = new EventEmitter();

    public constructor(@Inject(MAT_DIALOG_DATA) public data,
        public dialogRef: MatDialogRef<DeviceModalComponent>,
        private deviceService: DevicesService) { }

    ngOnInit(): void {
        this.getDeviceInfo();

    }

    getDeviceInfo() {
        if (this.data.prePopulatedData) {
            this.dataToEdit = true;

            this.deviceInfo = new FormGroup({
                'name': new FormControl(
                    this.data.prePopulatedData.name,
                    Validators.required),
                'longitude': new FormControl(
                    this.data.prePopulatedData.longitude,
                    Validators.required),
                'latitude': new FormControl(
                    this.data.prePopulatedData.latitude,
                    Validators.required),
            });

        } else {
            this.deviceInfo = new FormGroup({
                'name': new FormControl('', Validators.required),
                'longitude': new FormControl('', Validators.required),
                'latitude': new FormControl('', Validators.required),
            });
        }
    }

    private coordinatesSent(message) {
        this.deviceInfo.controls['latitude'].setValue(message.newLat.toString());
        this.deviceInfo.controls['longitude'].setValue(message.newLong.toString());
    }

    private doneClicked() {
        if (this.dataToEdit === true) {
            this.deviceService.save(this.deviceInfo.value, false, this.data.id);
        } else {
        this.deviceService.save(this.deviceInfo.value, true);
        }
    }

    private cancelClicked() {
        this.dialogRef.close();
    }
}
