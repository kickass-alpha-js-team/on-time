# In|Time
## Team details
### Team Name
- MaNi-MaNi Technologies
### Team members: 
- **Maria Dineva** (Username in telerikacademy.com - maria.dineva)
- **Nikolay Vassilev** (Username in telerikacademy.com - hldd3n)
## Used assets in the project
- TypeScript
- TSLint
- Angular 7
- Bootstrap 4
- Angular Material
- Kendo UI
- Leaflet & OpenStreet Map
- amCharts 4
### Project Information
1. Run **npm install**
2. With **ng serve** you can start the server. Then navigate to **http://localhost:4200/**. The app will automatically reload if you apply any changes to the source files.
### Project Description:
Our project is an application which eases the process of monitoring and controlling traffic by traffic engineers. It makes use of one of the latest devices of the company Rhythm Engineering - InTime. The device is used to detect drivers’ mobile devices by capturing the MAC addresses of all wireless devices (including cell phone in vehicles). After the data is retrieved, the travel time between these InTime devices is calculated by performing the delta between two point i.e. two intersections. Our application provides visual representation of this travel time. Our users can choose between a pre-set (by administrators) travel-time measuring devices for a chosen period of time. For the purpose of better visualization we have used tables, charts and maps so that the user can get the best experience retrieving the necessary data. We have tried to provide the most straightforward and uncomplicated view so that traffic engineers get the information they need without any wondering as to where to search for what they need, as well as without any distraction by unnecessary details. 
- **Public** - It is accessible to all unauthenticated users. These are the login and register forms.
- **Private view-users' part** - This is the part available to users who are not administrators, i.e. traffic engineers. It is accessible only after authentication. The so called 'view users' can access only the main part of the application - these are the table reports and chart reports found on route '/intime'. On that route they can find a pre-set map and a button to create a new travel-time table. Users should give it a name and choose the period and devices they would like to monitor. When done, users get a view of a table with the information about the travel time between each of the devices in the report. The map changes according to the selected devices. After creating a table they can also create a chart which would give them the opportunity to compare the travel time between two devices for the chosen period of time with the same period of time from another day (eg. compare travel time between two intersections today from 5p.m. to 6p.m. with the same period for yesterday and the day before) View users can perform CRUD operations on both types of reporting.
- **Private administrator's part** - It is accessible only after authentication and administration authorization. Just like view users,administrators have access to the table and chart reports, and can perform CRUD operations on them. Administrators can also create view users (by registering them with an email and password) and retrieve information about all current view users. Administrators can add travel-time measuring devices (by giving them names and choosing their coordinates from a map). They can also edit and delete devices. Administrators also have access to an audit log which records all major changes made by both administrators and view users (CRUD operations on users, devices, table reports and chart reports).

