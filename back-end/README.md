# In|Time
## Team details
### Team Name
- MaNi-MaNi Technologies
### Team members: 
- **Maria Dineva** (Username in telerikacademy.com - maria.dineva)
- **Nikolay Vassilev** (Username in telerikacademy.com - hldd3n)
## Used assets in the project
- TypeScript
- TSLint
- NestJS
- TypeORM
- MariaDB
- MySQL
- GitLab
- Trello
### Project Information
1. Run **npm install**
2. With **npm start** you can start the server to provide access to the routes. In order to run it in watch mode, you can use **npm run start:dev** command.
### Project Description:
Our back-end consists consists of three parts:
- **Public** - It is accessible to all unauthenticated users. These are the login and register routes.
- **Private view-users' part** - It is accessible only after authentication. View users can access only the main part of the application - these are the table reports and chart reports. View users can perform CRUD operations on both types of reporting.
- **Private administrator's part** - It is accessible only after authentication and administration authorization. Administrators have access to the table and chart reports, and can perform CRUD operations on them. Administrators can also create view users (by registering them with an email and password) and retrieve information about all current vie users. Administrators can add travel-time measuring devices (by giving them names and choosing their coordinates from a map). They can also edit and delete devices. Administrators also have access to an audit log which records all major changes made by both administrators and view users (CRUD operations on users, devices, table reports and chart reports).