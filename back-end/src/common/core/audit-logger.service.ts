import { User } from './../../data/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuditLog } from '../../data/entities/audit-log.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AuditLoggerService {
    constructor( @InjectRepository(AuditLog)
    private readonly auditLogRepository: Repository<AuditLog>) { }

    async getAll(admin: User) {
        return await this.auditLogRepository.find({ where: { userId: admin.id } });
      }

    async addNewLog(change: string, user: User) {
        const auditLog: AuditLog = new AuditLog();
        auditLog.change = change;
        auditLog.user = user;
        auditLog.date = new Date();

        await this.auditLogRepository.create(auditLog);
        await this.auditLogRepository.save(auditLog);
    }
}
