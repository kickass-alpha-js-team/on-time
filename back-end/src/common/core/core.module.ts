import { AuditLog } from './../../data/entities/audit-log.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { Module } from '@nestjs/common';
import { User } from './../../data/entities/user.entity';
import { AuditLoggerService } from './audit-logger.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, AuditLog])],
  providers: [UsersService, AuditLoggerService],
  exports: [UsersService, AuditLoggerService],
})
export class CoreModule { }
