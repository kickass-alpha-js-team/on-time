import { AuthModule } from '../auth/auth.module';
import { CoreModule } from '../common/core/core.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AuditLoggerController } from './audit-logger.controller';
import { AuditLog } from '../data/entities/audit-log.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AuditLog]), CoreModule, AuthModule],
  providers: [],
  exports: [],
  controllers: [AuditLoggerController],
})
export class AuditLoggerModule {}
