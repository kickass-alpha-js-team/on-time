import { Controller, Get, UseGuards } from '@nestjs/common';
import { AdminGuard } from '../common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from '@nestjs/common';
import { AuditLoggerService } from '../common/core/audit-logger.service';

@Controller('audit-logger')
export class AuditLoggerController {

  constructor(
    private readonly auditLoggerService: AuditLoggerService,
  ) { }

  @Get()
  @UseGuards(AuthGuard(), AdminGuard)
  all(@Request() req) {
    return this.auditLoggerService.getAll(req.user);
  }
}