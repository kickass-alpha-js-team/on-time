import { MigrationInterface, QueryRunner } from "typeorm";

export class initial1549310784793 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `audit_logs` (`id` varchar(255) NOT NULL, `change` varchar(255) NOT NULL, `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `start_dates` CHANGE `dateInMilliseconds` `dateInMilliseconds` bigint NULL");
        await queryRunner.query("ALTER TABLE `chart_reports` DROP FOREIGN KEY `FK_a3f88a9fcfb779ed8c855ec7b4c`");
        await queryRunner.query("ALTER TABLE `chart_reports` CHANGE `tableReportId` `tableReportId` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `table_reports` DROP FOREIGN KEY `FK_6ff2f80f2548e495ac26587dbee`");
        await queryRunner.query("ALTER TABLE `table_reports` CHANGE `userId` `userId` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_738b377b4eed6fc1e1c3792cdb0`");
        await queryRunner.query("ALTER TABLE `users` CHANGE `adminUserId` `adminUserId` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `chart_reports` ADD CONSTRAINT `FK_a3f88a9fcfb779ed8c855ec7b4c` FOREIGN KEY (`tableReportId`) REFERENCES `table_reports`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `table_reports` ADD CONSTRAINT `FK_6ff2f80f2548e495ac26587dbee` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_738b377b4eed6fc1e1c3792cdb0` FOREIGN KEY (`adminUserId`) REFERENCES `users`(`id`)");
        await queryRunner.query("ALTER TABLE `audit_logs` ADD CONSTRAINT `FK_cfa83f61e4d27a87fcae1e025ab` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `audit_logs` DROP FOREIGN KEY `FK_cfa83f61e4d27a87fcae1e025ab`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_738b377b4eed6fc1e1c3792cdb0`");
        await queryRunner.query("ALTER TABLE `table_reports` DROP FOREIGN KEY `FK_6ff2f80f2548e495ac26587dbee`");
        await queryRunner.query("ALTER TABLE `chart_reports` DROP FOREIGN KEY `FK_a3f88a9fcfb779ed8c855ec7b4c`");
        await queryRunner.query("ALTER TABLE `users` CHANGE `adminUserId` `adminUserId` varchar(255) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_738b377b4eed6fc1e1c3792cdb0` FOREIGN KEY (`adminUserId`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `table_reports` CHANGE `userId` `userId` varchar(255) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `table_reports` ADD CONSTRAINT `FK_6ff2f80f2548e495ac26587dbee` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `chart_reports` CHANGE `tableReportId` `tableReportId` varchar(255) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `chart_reports` ADD CONSTRAINT `FK_a3f88a9fcfb779ed8c855ec7b4c` FOREIGN KEY (`tableReportId`) REFERENCES `table_reports`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `start_dates` CHANGE `dateInMilliseconds` `dateInMilliseconds` bigint NULL DEFAULT 'NULL'");
        await queryRunner.query("DROP TABLE `audit_logs`");
    }

}
