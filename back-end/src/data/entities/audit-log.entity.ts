import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';

@Entity({ name: 'audit_logs' })
export class AuditLog {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    change: string;

    @CreateDateColumn()
    date: Date;

    @ManyToOne(type => User, user => user.auditLogs, { eager: true })
    user: User;
}
