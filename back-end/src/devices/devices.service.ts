import { AuditLoggerService } from './../common/core/audit-logger.service';
import { UpdateDeviceDTO } from './../models/devices/update-device.dto';
import { CreateDeviceDTO } from '../models/devices/create-device.dto';
import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Device } from 'src/data/entities/device.entity';
import { User } from 'src/data/entities/user.entity';
import { TableReport } from 'src/data/entities/table-report.entity';

@Injectable()
export class DevicesService {
    constructor(
        @InjectRepository(Device)
        private readonly devicesRepository: Repository<Device>,
        @InjectRepository(User)
        private readonly usersRepository: Repository<User>,
        @InjectRepository(TableReport)
        private readonly tableReportsRepository: Repository<TableReport>,
        private readonly auditLoggerService: AuditLoggerService) { }

    async create(user: User, device: CreateDeviceDTO): Promise<Device> {
        if (!user.isAdmin) {
            throw new BadRequestException('UnAuthorized!');
        }

        const deviceFoundByName = await this.foundDevice({ where: { name: device.name } });

        if (deviceFoundByName) {
            throw new BadRequestException('This device already exists!');
        }

        const createdDevice = new Device();
        createdDevice.name = device.name;
        createdDevice.longitude = device.longitude;
        createdDevice.latitude = device.latitude;
        createdDevice.users = [user];

        await this.devicesRepository.create(createdDevice);
        await this.devicesRepository.save([createdDevice]);

        const loggerMsg: string = `New device with name ${device.name} was added.`;
        this.auditLoggerService.addNewLog(loggerMsg, user);

        return createdDevice;
    }

    async findAll(req): Promise<Device[]> {
        const user = await this.usersRepository.findOne(
            {
                relations: ['adminUser'],
                where: { id: req.user.id },
            });

        const admin = await this.usersRepository.findOne(
            {
                relations: ['devices'],
                where: { id: user.adminUser.id },
            });
        const devices = admin.devices;
        return devices;
    }

    async findOne(idDevice): Promise<Device> {
        const foundDevice = await this.foundDevice({ where: { id: idDevice } });

        if (!foundDevice) {
            throw new BadRequestException('This device does not exist!');
        }

        return foundDevice;
    }

    async update(user: User, idDevice: string, updateDeviceDTO: UpdateDeviceDTO): Promise<Device> {

        const deviceFoundById = await this.foundDevice({ where: { id: idDevice } });

        if (!deviceFoundById) {
            throw new BadRequestException('This device does not exist!');
        }

        const deviceFoundByName = await this.foundDevice({ where: { name: updateDeviceDTO.name } });

        if (deviceFoundByName && (deviceFoundByName.id !== deviceFoundById.id)) {
            throw new BadRequestException('Such device name already exist!');
        }

        if (updateDeviceDTO.name) {
            deviceFoundById.name = updateDeviceDTO.name;
        }

        if (updateDeviceDTO.longitude) {
            deviceFoundById.longitude = updateDeviceDTO.longitude;
        }

        if (updateDeviceDTO.latitude) {
            deviceFoundById.latitude = updateDeviceDTO.latitude;
        }

        await this.devicesRepository.update(idDevice, deviceFoundById);
        await this.devicesRepository.save(deviceFoundById);

        const loggerMsg = `Device with name ${updateDeviceDTO.name} was updated.`;

        this.auditLoggerService.addNewLog(loggerMsg, user);

        return deviceFoundById;
    }
    async remove(user, idDevice): Promise<Device> {

        const deviceFoundById = await this.foundDevice({ where: { id: idDevice } });

        if (!deviceFoundById) {
            throw new BadRequestException('Such device does not exist!');
        }

        await this.devicesRepository.delete(idDevice);

        const loggerMsg = `Device with name ${deviceFoundById.name} was deleted.`;

        this.auditLoggerService.addNewLog(loggerMsg, user);

        return deviceFoundById;
    }
    private async foundDevice(query: object) {

        return await this.devicesRepository.findOne(query);
    }
}